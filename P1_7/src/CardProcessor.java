import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;

import cards.Card;


public class CardProcessor implements CardProcessor_I
{
    private Map<Card.Rank, Set<Card>> cards_;
    private int previousTriplets_;
    
    public CardProcessor()
    {
        reset();
    }
    
    /**
     * @require card != null
     * @require card not in CardProcessor
     * @return is fourth triplet found
     */
    @Override
    public boolean process(Card card)
    {
        if(card == null)
            throw new NullPointerException("Can't work with null!");
        
        Set<Card> l = getSet(card.getRank());
        
        if(l.contains(card))
            throw new IllegalArgumentException("Duplicate card " + card + "!");
        
        l.add(card);
        
        if(l.size() == 3)
        {
            printCards(l);
            System.out.println();
            ++previousTriplets_;
            l.clear();
        }
        
        if(previousTriplets_ == 4)
        {
            for(Set<Card> s : cards_.values())
            {
                printCards(s);
            }
            System.out.println();
            
            reset();
            
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public final void reset()
    {
        cards_ = new HashMap<>();
        previousTriplets_ = 0;
    }
    
    private Set<Card> getSet(Card.Rank r)
    {
        if(!cards_.containsKey(r))
        {
            cards_.put(r, new HashSet<Card>());
        }
        
        return cards_.get(r);
    }
    
    private void printCards(Set<Card> s)
    {
        for(Card c : s)
        {
            System.out.print(c + " ");
        }
    }
}
