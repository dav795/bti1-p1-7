/*
 * VCS:
 *  bitbucket.org/schaefers/P1_A7x1_BlueJ_Pilot.git     (valid)
 *  bitbucket.org/schaefers/LabExam_S14S_TI1P1.git      (old/before/inoperative/invalid)
 */


import cards.*;


/**
 * A7x1: ... - see task
 * 
 * @author   Michael Schäfers ; P1@Hamburg-UAS.eu 
 * @version  2016/01/02
 */
/**
 * <ul></ul>
 * Das Interface {@link CardProcessor_I}
 * <ul>
 *     <li>beschreibt eine CardProcessor und</li>
 *     <li>definiert die Funktionalit&auml;t m&ouml;glicher Implementierungen
 *         und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse <code><strong>CardProcessor</strong></code> muss
 * <ul>
 *     <li>einen Konstruktor aufweisenen, der der folgenden Signatur gen&uuml;gt:<br />
 *         <code>CardProcessor()</code></li>
 * </ul>
 * Eine genaue Auflistung der Anforderungen an die zu implemntierende Klasse findet sich auf dem Aufgabenzettel.
 */
public interface CardProcessor_I {
    
    
    /**
     * <code>public boolean <strong>process</strong>( Card </code><i>card</i> <code>)</code><br />
     * <br />
     * {@link #process(Card)} verarbeitet eine (Spiel-)Karte.
     * Die als Parameter &uuml;bergebene(n) Karte(n) wird(werden) zun&auml;chst intern gespeichert.
     * Sobald ein Drilling (3 Karten vom gleichen Rang) vorliegt,
     * soll dieser Drilling (also die entsprechenden 3 Karten) ausgegeben werden
     * und zwar nebeneinander bzw. einzeilig. Die Ausgabe wird also mit einem Zeilenumbruch abgeschlossen.
     * Danach sind die 3 Karten aus dem internen Speicher zu l&ouml;schen.
     * Sobald der vierte Drilling ausgegeben wurde, sind (auf der n&auml;chsten Zeile) alle restlichen Karten auszugeben
     * (ebenfalls wieder  nebeneinander bzw. einzeilig. Die Ausgabe wird wieder mit einem Zeilenumbruch abgeschlossen.)
     * @param card bestimmt die (neue) Karte, die zu verarbeiten ist.
     * @return Erfolgsmeldung <br />
     *         bzw. <strong><code>true</code></strong>, wenn mit der als Parameter &uuml;bergebenen Karte der vierte Drilling gebildet werden kann bzw. ausgegeben wird
     *         und sonst <strong><code>false</code></strong>.
     */
    boolean process( final Card card );
    
    
    /**
     * <code>public void <strong>reset</strong>()</code><br />
     * <br />
     * {@link #reset()} l&ouml;scht alle (internen) gespeicherten Karten.
     */
    void reset();
    
}//interface