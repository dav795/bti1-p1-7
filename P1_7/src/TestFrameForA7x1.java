/*
 * VCS:
 *  bitbucket.org/schaefers/P1_A7x1_BlueJ_Pilot.git     (valid)
 *  bitbucket.org/schaefers/LabExam_S14S_TI1P1.git      (old/before/inoperative/invalid)
 */


import cards.*;


public class TestFrameForA7x1 {
    
    public static void main( final String... unused ){
        
        System.out.printf( "ACHTUNG!: Dies ist ein zufallsbasierter Test\n" );
        System.out.printf( "\n\n" );
        
        
        System.out.printf( "Test1\n" );
        final CardProcessor_I cp = new CardProcessor();
        Deck deck = new Deck();
        while( ! cp.process( deck.deal() ));
        System.out.printf( "\n\n" );
        
        
        System.out.printf( "Test2\n" );
        cp.reset();
        System.out.printf( " -- \"reset()\" ---\n" );
        deck = new Deck();                                                      // new deck
        for (int i=13; --i>=0; ){ cp.process( deck.deal() ); }                  // <=2*9=18 => 13-9=4 => min. 4 trips left
        System.out.printf( "\n\n" );
        
        
        System.out.printf( "Test3\n" );
        cp.reset();
        System.out.printf( " -- \"reset()\" ---\n" );
        while( ! cp.process( deck.deal() ));
        
    }//method()
    
}//class